"use strict";

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var capitalize = function capitalize(str) {
  return str[0].toUpperCase() + str.split("").slice(1).join("");
};

var getName = function getName(_ref) {
  var first = _ref.first,
      last = _ref.last;
  return capitalize(first) + " " + capitalize(last);
};

var DatingApp = function (_React$Component) {
  _inherits(DatingApp, _React$Component);

  function DatingApp(props) {
    _classCallCheck(this, DatingApp);

    var _this = _possibleConstructorReturn(this, (DatingApp.__proto__ || Object.getPrototypeOf(DatingApp)).call(this, props));

    _this.state = {
      counter: 0,
      match: undefined,
      loadNext: false
    };
    return _this;
  }

  _createClass(DatingApp, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      fetch("https://randomuser.me/api").then(function (r) {
        return r.json();
      }).then(function (_ref2) {
        var _ref2$results = _slicedToArray(_ref2.results, 1),
            _ref2$results$ = _ref2$results[0],
            name = _ref2$results$.name,
            dob = _ref2$results$.dob,
            thumbnail = _ref2$results$.picture.thumbnail;

        return _this2.setState({
          match: { name: getName(name), age: dob.age, image: thumbnail }
        });
      });
    }
  }, {
    key: "componentDidUpdate",
    value: function componentDidUpdate(prevProps, prevState) {
      var _this3 = this;

      if (prevState.loadNext === false && this.state.loadNext) {
        fetch("https://randomuser.me/api").then(function (r) {
          return r.json();
        }).then(function (_ref3) {
          var _ref3$results = _slicedToArray(_ref3.results, 1),
              _ref3$results$ = _ref3$results[0],
              name = _ref3$results$.name,
              dob = _ref3$results$.dob,
              large = _ref3$results$.picture.large;

          return _this3.setState({
            match: { name: getName(name), age: dob.age, image: large },
            loadNext: false
          });
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var _this4 = this;

      var _state = this.state,
          match = _state.match,
          counter = _state.counter,
          loadNext = _state.loadNext;

      return React.createElement(
        "div",
        { className: "container" },
        React.createElement(
          "div",
          { className: "header" },
          React.createElement(
            "span",
            { style: { fontSize: "18px", fontWeight: "bold" } },
            "Gender Neutral Dating App"
          ),
          " ",
          React.createElement(
            "span",
            {
              style: {
                background: counter >= 5 ? "#FF6F00" : "white",
                padding: "4px 8px",
                borderRadius: "4px",
                fontSize: "14px",
                color: "black"
              }
            },
            this.state.counter
          )
        ),
        !loadNext && match ? React.createElement(
          "div",
          { className: "center" },
          React.createElement("img", {
            style: { width: "128px", height: "128px", borderRadius: "50%" },
            src: match.image
          }),
          React.createElement(
            "span",
            { style: { fontSize: "32px" } },
            match.name
          ),
          React.createElement(
            "span",
            { style: { fontSize: "14px" } },
            "(" + match.age + ")"
          )
        ) : React.createElement(
          "div",
          { className: "center" },
          React.createElement(
            "span",
            null,
            "Loading..."
          )
        ),
        React.createElement(
          "div",
          { style: { display: "flex" } },
          React.createElement(
            "div",
            {
              style: {
                flex: 1,
                textAlign: "center",
                display: "flex",
                alignItems: "center",
                justifyContent: "center",
                cursor: "pointer",
                color: loadNext ? "#757575" : "white",
                background: "#424242",
                height: "60px"
              },
              onClick: function onClick() {
                return !loadNext ? _this4.setState({ loadNext: true }) : null;
              }
            },
            "No"
          ),
          React.createElement(
            "div",
            {
              style: {
                flex: 1,
                display: "flex",
                alignItems: "center",
                textAlign: "center",
                cursor: "pointer",
                justifyContent: "center",
                color: loadNext || counter >= 5 ? "#757575" : "white",
                background: loadNext || counter >= 5 ? "#424242" : "#FF6F00",
                height: "60px"
              },
              onClick: function onClick() {
                return !loadNext && counter < 5 ? _this4.setState({ counter: counter + 1, loadNext: true }) : null;
              }
            },
            "Yes"
          )
        )
      );
    }
  }]);

  return DatingApp;
}(React.Component);

ReactDOM.render(React.createElement(DatingApp, null), document.getElementById("root"));