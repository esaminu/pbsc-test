const capitalize = str => {
  return (
    str[0].toUpperCase() +
    str
      .split("")
      .slice(1)
      .join("")
  );
};

const getName = ({ first, last }) => `${capitalize(first)} ${capitalize(last)}`;

class DatingApp extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      counter: 0,
      match: undefined,
      loadNext: false
    };
  }

  componentDidMount() {
    fetch("https://randomuser.me/api")
      .then(r => r.json())
      .then(({ results: [{ name, dob, picture: { thumbnail } }] }) =>
        this.setState({
          match: { name: getName(name), age: dob.age, image: thumbnail }
        })
      );
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.loadNext === false && this.state.loadNext) {
      fetch("https://randomuser.me/api")
        .then(r => r.json())
        .then(({ results: [{ name, dob, picture: { large } }] }) =>
          this.setState({
            match: { name: getName(name), age: dob.age, image: large },
            loadNext: false
          })
        );
    }
  }

  render() {
    let { match, counter, loadNext } = this.state;
    return (
      <div className="container">
        <div className="header">
          <span style={{ fontSize: "18px", fontWeight: "bold" }}>
            Gender Neutral Dating App
          </span>{" "}
          <span
            style={{
              background: counter >= 5 ? "#FF6F00" : "white",
              padding: "4px 8px",
              borderRadius: "4px",
              fontSize: "14px",
              color: "black"
            }}
          >
            {this.state.counter}
          </span>
        </div>
        {!loadNext && match ? (
          <div className="center">
            <img
              style={{ width: "128px", height: "128px", borderRadius: "50%" }}
              src={match.image}
            />
            <span style={{ fontSize: "32px" }}>{match.name}</span>
            <span style={{ fontSize: "14px" }}>{`(${match.age})`}</span>
          </div>
        ) : (
          <div className="center">
            <span>Loading...</span>
          </div>
        )}
        <div style={{ display: "flex" }}>
          <div
            style={{
              flex: 1,
              textAlign: "center",
              display: "flex",
              alignItems: "center",
              justifyContent: "center",
              cursor: "pointer",
              color: loadNext ? "#757575" : "white",
              background: "#424242",
              height: "60px"
            }}
            onClick={() =>
              !loadNext ? this.setState({ loadNext: true }) : null
            }
          >
            No
          </div>
          <div
            style={{
              flex: 1,
              display: "flex",
              alignItems: "center",
              textAlign: "center",
              cursor: "pointer",
              justifyContent: "center",
              color: loadNext || counter >= 5 ? "#757575" : "white",
              background: loadNext || counter >= 5 ? "#424242" : "#FF6F00",
              height: "60px"
            }}
            onClick={() =>
              !loadNext && counter < 5
                ? this.setState({ counter: counter + 1, loadNext: true })
                : null
            }
          >
            Yes
          </div>
        </div>
      </div>
    );
  }
}

ReactDOM.render(<DatingApp />, document.getElementById("root"));
